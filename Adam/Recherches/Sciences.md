## Ressources
J'ai trouvé de nombreuses ressources pour comprendre l'évolution des techiques et des sciences à l'époque. Ca me semble particulièrement pertinent.

### The Shock of the Old: Technology and Global History since 1900
[10€ sur amazon](https://www.amazon.co.uk/cart/smart-wagon?newItems=a23259bf-0b8c-4191-a5f3-24cea0af9b5a,1&ref_=sw_refresh)
[9€42 ici](https://www.chasse-aux-livres.fr/prix/1861973063/the-shock-of-the-old-david-edgerton?query=The%20Shock%20of%20the%20Old%3A%20Technology%20and%20Global%20History%20since%201900)
Etudier les technologies de l'époque par rapport à leur utilisation. Cet ouvrage examine comment les innovations technologiques du passé, y compris celles de la fin du XIXe siècle, continuent d'influencer notre conception des sciences et des techniques.
[Bibli !](https://catalogue.bm-lyon.fr/ark:/75584/pf0002152415)

### Technological Revolutions and Financial Capital
[20€ sur Play Livres](https://play.google.com/store/books/details/C_Perez_Technological_Revolutions_and_Financial_Ca?id=QPRgvx_cD-MC&hl=en_GB)
Lien entre le capitalisme financier et le développement des technologies. C'est pas trop long en plus.

### Inventing the Industrial Revolution
[Gratuit ? sur Play Livres](https://books.google.fr/books?id=aSeD_l5eX00C&printsec=copyright&redir_esc=y#v=onepage&q&f=false)
Ce livre se penche sur la manière dont le système des brevets en Angleterre a influencé l'innovation technologique et scientifique, ce qui a jeté les bases de la révolution industrielle.

### The Second Industrial Revolution, 1870-1914
[Gratuit ici ?](https://fr.scribd.com/document/557851159/Creating-the-Twentieth-Century-Vaclav-Smil)
Ca l'air très complet mais c'est un peu n'imp les éditions.
Regarder ici surtout : (https://www.goodreads.com/search?page=2&q=Vaclav+Smil&qid=QqtOstC2Qw&tab=books)

### L'Événement anthropocène
[10€ sur Play Livres](https://play.google.com/store/books/details?pcampaignid=books_read_action&id=lWgJDAAAQBAJ)
[Dans toutes les biblis mdr](https://catalogue.bm-lyon.fr/ark:/75584/pf0002434621?posInSet=2&queryId=5ada16ca-f61c-448b-a39a-58c3926e101e)

### Histoire des sciences et des savoirs - Tome 2, Modernité et globalisation
[11€ sur Décitre](https://www.decitre.fr/livres/histoire-des-sciences-et-des-savoirs-9782757879788.html)
[Bibli Part Dieu](https://catalogue.bm-lyon.fr/ark:/75584/pf0002750644?posInSet=65&queryId=d12f6165-88e4-4da8-8533-201d599139db)
Se donnant comme " la Modernité ", les années 1770 à 1914 sont le temps de l'industrialisation et de l'expansion impériale et coloniale.

### La science de la science Bourdieu
[6€ ici](https://www.chasse-aux-livres.fr/prix/2912107148/science-de-la-science-et-reflexivite-pierre-bourdieu?query=Science%20de%20la%20science%20et%20r%C3%A9flexivit%C3%A9)
[Revue gratuite ici](https://psychaanalyse.com/pdf/LA_SCIENCE_DE_LA_SCIENCE_CHEZ_PIERRRE_BOURDIEU.pdf)

### L'idée fondamentale du Positivisme et ses conséquences logiques
[Extrait ? ici](https://www.persee.fr/doc/phlou_0776-5541_1894_num_1_2_1369)
Réflexion philosophique sur le positivisme.

### Technological Fix: How People Use Technology to Create and Solve Problems
[23€ ici](https://www.kriso.ee/technological-fix-how-people-use-technology-db-97802035013512e.html?id=dyYxdmes)
The term "technological fix" should mean a fix provided by technology--a solution for all of our problems, from medicine and food production to the environment and business. Instead, technological fix has come to mean a cheap, quick fix using inappropriate technology that usually creates more problems than it solves. This collection sets out the distinction between a technological fix and a true technological solution.

### A contre-science
[15€ epub](https://www.seuil.com/ouvrage/a-contre-science-dominique-pestre/9782021079203)
[13€ livre](https://www.chasse-aux-livres.fr/prix/2021079201/a-contre-science-dominique-pestre?query=A%20contre-science)

### En plus
https://www.persee.fr/doc/raipr_0033-9075_1992_num_104_1_3076_t1_0159_0000_2
https://www.editions-harmattan.fr/livre-le_concept_de_science_positive_ses_tenants_et_ses_aboutissants_dans_les_structures_anthropologiques_du_positivisme_angele_kremer_marietti-9782296035003-24001.html
https://psychaanalyse.com/pdf/LA_SCIENCE_DE_LA_SCIENCE_CHEZ_PIERRRE_BOURDIEU.pdf


### Recap
