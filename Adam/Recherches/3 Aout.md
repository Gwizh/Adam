*Je commence à regretter l'idée de faire deux parties. Il va falloir que je décide, où alors je fais deux parties*
En effet, there will be blood m'a fait me rendre compte qu'on peut être pertinent sans séparer l'éloge de la critique.
Mais après ça reste marquant de créer une démarquation violente entre les parties.

*Hypothèse deux parties*
En résumant la première partie à ce que voit un futur "historien" de l'histoire, on peut quand même aborder plein de choses qui peuvent sembler tenir de lui et en plus réellement donner un sens d'étapes claires et de batailles. Adam apparaît comme la personne qui aura répondu à tous les événements problématiques avec brio et on ignore tout de ce qui se cachait derrière. L'historien est clueless. Je pense qu'on peut quand même faire du twbb là-dedans, mais l'idée n'est pas identique car je dois montrer que c'est sa nature profonde en restant subtil dans la première partie, il faut pas que l'historien l'évoque volontairement. Tout doit apparaître ensuite, par petites touches et en reprenant les batailles comme support de narration (on peut donc se créer une timeline moins linéaire, plus réaliste).

*Euuuh*
Reprendre la façon de procéder de l'entrepreneur de l'autre histoire que j'avais ? Pour qui ? Adam ? John ? Bennie ?

*Changer la quatrième de couverture pour vraiment parler d'Adam et de son thriomphe, il faut que ça soit vraiment cynique*