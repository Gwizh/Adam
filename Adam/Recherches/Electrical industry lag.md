# British Electrical Industry Lag
**Il faut ABSOlument que j'utilise ça pour le chapitre Guerre3, c'est absolument parfait !**

### Prix de l'électricité
Une très bonne idée (l'histoire est ainsi faite) : mettre le prix de l'électricité au prix du gaz, à perte, pour attirer des clients et montrer que tout est merveilleux. 
The English patent-owning company established rates for Holborn consumers calculated to create good will and favorable publicity. From April until July, 1882, the station supplied street lighting without charge to the City authorities; for the next six months the rates were the same as gas. Individual arrangements were made with private consumers, but the plan for Holborn kept the price near gas even if it meant no profit.' Electricity supplied to customers at rates comparable to gas undoubtedly cost the station at least twice the selling price in 1882.

### Intéraction entre autorités locales et investisseurs privés
Du fait des précédents excès des monopoles dans les domaines de l'eau et du gaz (*à voir*) et de la montée des mouvements socialistes, le gouvernement voulait laisser aux autorités locales le contrôle sur les installations électriques. Il ne fallait plus laisser tous les droits aux industriels. Pour ce faire, ils ont créés entre autres un système qui permettait aux industriels d'innover et de construire des infrastructures pendant un temps limité pour ensuite pouvoir vendre (?) aux autorités. I've got a soucis of compréstanding.

### Provisional orders
Entre les deux et plus loin il ya beaucoup de détails sur l'electrical act que je ne comprends pas tout à fait, à piger.
    This action manifested atrend: in 1883 the Board of Trade
    granted sixty-nine provisional orders for central station systems and
    sixty-two were subsequently revoked; in 1884 only four were granted
    and all were revoked; and in 1885 none was issued.59

### En hiver, on galère
Une autre : tandis que les demandes en énergie vont augmenter en hiver, la législation est trop restrictive et risque d'empêcher de finir les installations nécessaires. Battle pour la survie du peuple.
    _Yet, as early as July 1, electrical manufacturers and light companies
    were not making preparations for the anticipated winter demand.4" In
    September, 1882, disgruntled Brush shareholders questioned company
    policy.50 By the end of December electrical light company shares had
    dropped dramatically from their spring highs.51_

### Ça c'est absolument central et c'est super intéressant !
Ya vraiment une idée centrale qui est que la législation, voulant protéger le public des abus de la propriété qu'il y a eu dans les autres secteurs, ont voulu s'assurer que les municipalités pouvaient contrôler l'expansion des industries. L'idée défendue par les capitalistes étaient que les municipalités n'allaient pas innover et qu'il fallait leur laisser la main. En gros la bulle de l'électricité vient de là.

Ce que je comprend de ma première lecture c'est que les américains n'ont pas eu autant d'entraves politiques, l'innovation était poussée naturellement.

Et bien sûr il faut que je me renseigne sur (mdr c'est quoi ça du coup xD, ce sont vraiment des notes de merde :/)

### Financements et législations ne sont pas synchronisés !
Ya aussi une idée merveilleuse : celle qui veut que les financements privés ont augmentés dès 1887, avant même que la législation soit changée. Ça signifie que les investisseurs voyaient déjà le marché à la hausse et on peut vraiment faire une méga métaphore du courant là, le débit qui augmente malgré que ce soit toujours bloqué. (p37)