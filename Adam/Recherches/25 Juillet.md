### Des éclairs
Ne donne pas tant de contexte que ça. 

### Idées sur la structure du récit
Faire vraiment un récit classique de l'entrepreneur dans les premiers chapitres dans lesquels on pense vraiment qu'on va avoir la suite. Un peu comme dans Jojo, à la fin de la partie 1 : Jojo meurt. Choquède. Et puis il faut vraiment utiliser la guerre des courants pour faire croire que tous le livre se base sur ça. 

#### Premiers chapitres
Enfance d'Adam, le début de sa passion : ajouter plus d'expériences farfelues (et potentiellement stupides). Début de son entreprise avec Horvath : amitiée et fausse précarité. Adam passe pour l'entrepreneur compréhensif qui accueille les rebuts des méchants capitalistes. Guerre des courants et des brevets. Adam passe pour un grand bonhommme paternaliste et bon. Philantrope et collectionneur. La relation avec le frère est importante aussi dans la guerre du courant : il va sûrement falloir diviser les chapitres. On ressort de ces chapitres avec l'impression qu'Adam a gagné, qu'il ne lui reste rien à faire de plus.

#### Mettre des courants ici ??????? 
Ça ferrait sens dans l'idée de vraiment créer une rupture entre les deux parties. 

#### Ingénieur
Ce n'est pas assez intéressant en soi. Il faudrait plus utiliser les caractéristiques de Bennie pour créer un récit plus intéressant

#### Ouvrier
Tout est bon, plus qu'à finir

#### Feu 
Détective et affaire, faudrait lier tout ça mais on y arrive

#### Territoires
Pas besoin forcément d'en faire qq chose de long, ça peut être que du sous-entendu

#### Femme
L'histoire de Laura opposée à l'histoire d'Anna.
La maison est très similaire à celle de son père
Son fils s'intéresse aux mêmes choses et se prend pour un génie

#### Maladie et Mort 
Fusionner ? 

*Globalement il y a un truc qui me gène : la suite du récit est trop bizarre. Il faudrait vraiment voir quoi faire de la seconde moitié. Peut-être qu'un récit plus linéaire pourrait fonctionner finalement. En fait je suis tirraillé entre un récit classique, linéaire et des vignettes colorées. Du coup il ya 3 possibilités : linéaire puis vignettes, deux récits linéaires indépendants, deux récits linéaires qui se suivent vraiment* 

### Trucs à voir

#### Important
https://www.youtube.com/watch?v=wuqodfC9WwY
#### Optionnel
https://www.youtube.com/watch?v=8tuXdlGBbcg
https://www.youtube.com/watch?v=n1IzetXHttI
https://www.youtube.com/watch?v=BI4DR_uYKFU
https://www.youtube.com/watch?v=RhP-HdZJkIY