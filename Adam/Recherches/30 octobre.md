## It's been a little while
Sorry, j'ai eu une période sans Adam ni Vaultix, assez fou non ?
Mais je me suis focus sur l'économie avec le livre que ma passé Corentin et j'ai commencé à étudier Victoria 3.
Et aussi je pense que j'ai eu un blocage à cause du fait que je dois finir les livres pour la fin d'année. Ça aide pas trop.

## Sciences, techniques, technologies, innovation, progrès
Toutes ces notions sont cruciales pour vraiment cerner la deuxième révolution industrielle. Je les ai pourtant laissés de côté j'ai l'impression.

Si des courants dominants cherche à étudier le paternalisme industriel au sein de l'entrepreunariat de la fin du XIX siècle, il baigne aussi dans le grand bain de la révolution industrielle. Tous les premiers chapitres est le récit d'inventeurs qui vont triomphé technologiquement sur les autres. Je ne peux donc pas passer à côté de la façon dont tout ça se joue. Car si paternalisme il y a, c'est surtout grâce au fait que les savoirs et les techniques sont légitimés avec l'autorité que la science a pris à l'époque. Le scientisme et le rationalisme sont cruciaux dans ce livre, car ils sont les points de départ de l'analyse du paternalisme et du darwinisme social. Il faut donc étudier en profondeur comment les savoirs naissent et sont ensuite pris dans la notion plus grande de progrès.

D'autant que la destruction de la nature commence notamment à l'époque. Cette étude est cruciale pour comprendre si on aurait pu développer toutes les technologies de l'époque sans la domination de la nature qu'on a mis en place. Est-ce qu'on aurait pu inventer sans détruire ?

## A mettre dans Vaultix plz
[Collection de la bibli sur l'anthropologie économique](https://catalogue.bm-lyon.fr/search/N-EXPLORE-061cc3c8-cb45-4ec3-94a1-9d4619f0c814)

## Autres notions à creuser
[Bourses du travail](https://fr.wikipedia.org/wiki/Bourse_du_travail)
[Philosophie de l'éco](https://www.chasse-aux-livres.fr/prix/2804721183/les-racines-philosophiques-de-la-science-moderne-angele-kremer-marietti?query=Ang%C3%A8le%20Kremer%20Marietti)
