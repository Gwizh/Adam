### Bon ya eu le méga vocal hier, je vais essayer d'en parler
Donc en gros :
Partie 1 :
    Roman biographique d'Adam dans le monde du récit. On déroule toute sa vie avec une sorte d'admiration. Tous les bons côtés viennent de lui, tous les mauvais viennent de l'époque ou des gens autour de lui. C'est quelqu'un dans dans le futur de la diégèse qui l'écrit. Récit classique de l'entrepreneur mais où tout lui va car Adam est créer pour ce monde, créer pour que tout lui réussisse. On a donc l'enfance et la mort dedans (une vision très simple). Il faudrait sûrement faire plus de chapitre que ça pour créer un effet plus intéressant. Oh et je pense que j'ai vraiment pas besoin d'aller à fond dans tous les détails.
Partie 2 :
    Pas encore complètement défini mais ici la forme est beaucoup plus libre, on retrouve tout ce qui nous manquait dans la partie 1
    Et puis surtout Laura
    *Que faire du courant?*


### Capitalisme c'est la concurrence
Il faut qu'il soit en concurrence avec les autres entreprises, qu'il stresse de perdre la face. Il va négocier, tricher, signer, voler... En vrai il faudrait un capitaliste ennemi. 

### OK je viens de tester un peu
La différence entre biographie romancée et roman biographique est immense en pratique. Il faudra que je tranche. Je pense que le plus simple et le plus logique serait de faire un roman biographique comme Des Eclairs, j'ai pas besoin de parler d'archives même si ce serait super intéressant. Dans une biographie, on pourrait mettre de fausses notes de bas de page.