vicier

verbe transitif
Conjugaison

(latin vitiare, de vitium, vice)

    1. Entacher un acte juridique d'un défaut qui le rend nul : L'absence de date vicie le contrat.
    2. Littéraire. Polluer l'air, l'atmosphère : Les usines vicient l'air de leurs fumées.

    Synonymes :

    polluer - souiller

    Contraires :

    assainir - dépurer - épurer - filtrer - purifier
    3. Littéraire. Dénaturer, pervertir quelque chose : L'argent a tout vicié.

    Synonymes :

    annuler - corrompre - dénaturer - dévoyer - pervertir

    Contraires :

    amender - corriger - élever - redresser
