Ok donc globalement j'ai plusieurs nouvelles idées qu'il faut que j'introduise

### Les territoires
Il faut que je lui accorde une place plus centrale. Je dois aussi vraiement montrer que le racisme est structurel au libéralisme. 

### Lecture du tour du monde en 80 jours
Je tire deux choses de cette lecture :
 - Mon récit peut-être plus "fun". Je peux vraiment créer un parallèle avec Jack l'éventreur dans Le Feu par exemple, ajouter un personnage de détective qui soit vraiment intéressant, dialogant avec John et le prenant pour un abruti par exemple. Jouer la carte sherlock holmes ou juste polar de ces années-là. Je dois aussi rendre Adam plus intéressant, plus excentrique, plus passionnant. Dans l'Empire, je peux le rendre plus vivant en le faisant parler plus souvent typiquement. Je peux commenter les débats entre l'AC et le DC et surtout la fameuse partie sur le cable. Dans le Frère, je peux vraiment montrer les débats dans le parlement. Dans l'ouvrier j'avais ça déjà un peu.
 - Détails du racisme et du sexime : paternalisme qui aide les populations "sauvages" mais qui les dégrade complètement. Dilemne sur le fait que ces populations pratiquent le sacrifice humain. Doit-on les laisser s'entre-tuer, "nous" qui sommes responsables et raisonnés ? (2nd degré ofcourse)

### Lecture des ressources d'économie - Économie Néoclassique
Faut que je parle de la bourse, de la concurrence pure et parfaite et de l'entrepreneuriat dans ce sysyème. Il faut aussi que je lise les sources sur la situation de l'époque vu qu'il y avait une suite de crises. Electrical industry lag est bien là-dessus il donne masse de sources.

### Laura
Il faut qu'elle prenne une plus grosse place. Elle peut complètement changer les opinions d'Anna et de Richard.



### Nouvelles sources
https://api.parliament.uk/historic-hansard/commons/1886/may/14/the-irish-policy-of-the-government
https://fr.wikipedia.org/wiki/Lionel_Cohen
https://books.google.fr/books?id=Id05AQAAMAAJ&printsec=frontcover&hl=fr&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false
https://books.google.fr/books?id=g07Q9M4agp4C&pg=PA57&lpg=PA57&dq=%22Commercial+History+and+Review+of+1882,%22&source=bl&ots=BEDH2DooFS&sig=ACfU3U19QRYFYv3gRKmLHtE0j_AVRi_zlw&hl=fr&sa=X&ved=2ahUKEwiH_OGWi_r_AhVTVKQEHUBeB6AQ6AF6BAgkEAM#v=onepage&q&f=false
https://upload.wikimedia.org/wikipedia/commons/7/75/The_Economist_1883-02-24-_Vol_41_Supplement_%28IA_sim_economist_1883-02-24_41_supplement%29.pdf
https://www.jstor.org/stable/2114975
A trouver : Samuel Rezneck, Patterns of Thought and Action in an American Depres- sion 1882-1886, American Historical Review, LXI (1956), pp. 284-307.

Livres proches
https://www.babelio.com/livres/Echenoz-Des-eclairs/1354614#!
https://www.babelio.com/livres/Delbourg-Lhomme-aux-lacets-defaits/191593