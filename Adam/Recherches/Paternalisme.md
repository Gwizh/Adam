### Idée
Il faut critiquer la paternalisme de l'époque : c'était vraiment l'idée de base. La paternalisme de Zola et de Ferry qui souhaite vraiment de bon coeur aider les autres mais sans jamais les écouter, toujours à proposer des solutions pour apparaître comme le sauveur. 

C'est Adam, un pur produit de cette époque ! Il veut vraiment instaurer un pouvoir lié au mérite et non à des titres de noblesse. Pourtant, jamais il ne voit que la méthode qu'il force d'adopter n'est pas la seule qui existe et surtout que ça ne respecte pas les gens avec qui il travaille. 

Oui ça augmente leur niveau de vie. Il vaut mieux un père qu'un escroc mais la vraie bonté c'est de laisser les gens s'organiser par eux-mêmes. 

Donc dans ce cas là il faut vraiment que Richard Birch apparaisse comme un boss de son boulot, suffisament intelligent pour gérer tout lui-même. Il faut vraiment montrer la différence entre les syndicalistes jaunes et rouges. Richard serait quoi lui ?

Et bien sûr ça va avec le colonialisme, il veut créer des infrastructures électriques un peu partout dans les pays conquis pour aider les gens là-bas. Il va se faire de grandes marges là-bas car c'est payé directement par l'état ou les gouverneurs là-bas. La main-d'oeuvre est de plus bien moins chère (travail forcé). 

### Docu
Émile Zola était généralement en faveur du colonialisme et soutenait les idées expansionnistes de son époque. Ses écrits et discours montrent qu'il partageait certaines des opinions et des attitudes communes parmi les intellectuels français de l'époque, qui considéraient la colonisation comme une mission civilisatrice et justifiaient souvent les actions coloniales au nom du progrès et de la supériorité de la civilisation occidentale.

Dans ses écrits, notamment dans son roman "La Débâcle" publié en 1892, Zola évoque positivement la présence française en Algérie et en Afrique, et décrit la colonisation comme un moyen de propager la civilisation et d'apporter des bienfaits aux populations autochtones. Il a également soutenu la politique coloniale française en écrivant des articles dans des journaux et en prononçant des discours lors de conférences.

Cependant, il est important de souligner que les opinions de Zola sur le colonialisme n'étaient pas statiques et ont évolué au fil du temps. À la fin de sa vie, il a montré une plus grande sensibilité aux injustices et aux abus commis dans le cadre de la colonisation, notamment dans son célèbre article "J'accuse...!" publié en 1898, où il dénonçait l'affaire Dreyfus et critiquait le nationalisme exacerbé qui avait pris racine en France.

Il convient donc de noter que Zola a eu des opinions complexes sur le colonialisme, qui ont évolué au cours de sa vie et ont été influencées par les débats et les événements de son époque.

___

Jules Ferry avait une vision complexe des classes populaires et des prolétaires de son époque. En tant que politique républicain, il était préoccupé par les conditions de vie des travailleurs et cherchait à mettre en place des politiques sociales pour améliorer leur sort.

Ferry reconnaissait les difficultés et les inégalités auxquelles étaient confrontées les classes populaires et il défendait l'idée de l'égalité des chances. Il soutenait des réformes visant à améliorer l'éducation et à offrir des opportunités d'ascension sociale pour les travailleurs et leurs enfants. Son engagement pour l'éducation publique et la mise en place de l'école laïque, obligatoire et gratuite était motivé en partie par sa volonté de combattre l'analphabétisme et d'ouvrir les portes de la mobilité sociale.

Cependant, il est important de noter que la vision de Ferry était également influencée par certains préjugés et conceptions de son époque. Il pouvait avoir une approche paternaliste envers les classes populaires, considérant qu'il était du devoir des élites de les guider et de les éduquer. Il avait une certaine méfiance à l'égard des mouvements ouvriers et des luttes sociales radicales, préférant promouvoir des réformes graduelles et modérées plutôt que des changements révolutionnaires.

En résumé, la vision de Jules Ferry des classes populaires et des prolétaires était marquée par une préoccupation pour l'amélioration de leurs conditions de vie, en particulier par l'accès à l'éducation et à l'ascension sociale. Cependant, cette vision était teintée de certaines conceptions paternalistes et de méfiance envers les mouvements ouvriers radicaux.

___

La vision paternaliste de Jules Ferry à l'égard des classes populaires se manifestait par une approche où les élites politiques et sociales se considéraient comme des protecteurs et des guides des travailleurs et des prolétaires. Cette approche paternaliste était basée sur l'idée que les classes populaires avaient besoin de la direction et de l'éducation des classes dirigeantes pour s'améliorer.

Ferry considérait que les travailleurs avaient besoin d'être encadrés et éduqués par les élites afin de les guider vers une meilleure compréhension de leurs intérêts et de leur rôle dans la société. Il croyait en une forme de tutelle bienveillante, où les élites politiques et économiques se sentaient responsables du bien-être et de l'amélioration des conditions de vie des travailleurs.

Cette vision paternaliste pouvait parfois sous-estimer ou ignorer les capacités des classes populaires à s'organiser et à défendre leurs propres intérêts. Elle impliquait souvent une approche descendante où les décisions et les réformes étaient prises par les élites sans véritable consultation ou participation des travailleurs eux-mêmes.

Il est important de noter que cette vision paternaliste était largement répandue à l'époque de Jules Ferry et n'était pas spécifique à sa personne. Elle reflétait les conceptions sociales et politiques dominantes de l'époque, où l'élite éclairée était considérée comme ayant la responsabilité d'orienter et de guider les classes populaires vers le progrès.

Cependant, cette vision paternaliste était également contestée et critiquée par certains mouvements ouvriers et intellectuels qui revendiquaient l'autonomie et les droits des travailleurs à s'organiser et à lutter pour leurs propres intérêts.

___

On peut dire que la Troisième République en France, qui a duré de 1870 à 1940, comportait des éléments de paternalisme dans sa gouvernance. Cependant, il est important de noter que la Troisième République était un régime politique complexe et diversifié, et il y avait différents courants de pensée au sein de cette période.

Certains aspects du système politique de la Troisième République peuvent être considérés comme paternalistes. Par exemple, la promotion de l'éducation publique et l'expansion du système éducatif étaient basées sur l'idée que les classes populaires avaient besoin de l'enseignement et de la guidance de l'élite éclairée pour s'améliorer. Les élites politiques et intellectuelles se voyaient souvent comme des protecteurs et des guides des travailleurs, cherchant à les éduquer et à les civiliser selon leurs propres normes et valeurs.

En outre, le paternalisme pouvait être présent dans les politiques sociales et économiques de l'époque. Les réformes sociales, comme la création de la Sécurité sociale et l'amélioration des conditions de travail, étaient souvent mises en place par les élites politiques dans le but d'améliorer le bien-être des classes populaires. Cependant, ces réformes étaient souvent conçues et mises en œuvre sans une véritable participation ou consultation des travailleurs eux-mêmes.

Cependant, il est également important de noter que la Troisième République n'était pas entièrement caractérisée par le paternalisme. Au fil du temps, des mouvements ouvriers et des forces politiques se sont développés pour revendiquer les droits et la représentation des travailleurs, remettant en question l'approche paternaliste. Des organisations syndicales et des partis politiques de gauche ont émergé pour défendre les intérêts des travailleurs et lutter pour une plus grande autonomie.

En résumé, bien que la Troisième République en France comportait des éléments de paternalisme dans sa gouvernance, il est important de reconnaître que le paysage politique de cette époque était complexe, avec des mouvements ouvriers et des forces politiques qui revendiquaient l'autonomie et les droits des travailleurs.

### Ressources
https://www.cairn.info/aux-sources-du-roman-colonial-1863-1914--9782811139292.htm
https://www.cairn.info/aux-sources-du-roman-colonial-1863-1914--9782811139292-page-357.htm
https://www.nouvelobs.com/monde/afrique/20181016.OBS4004/colonialisme-quand-zola-confondait-le-maroc-et-la-tunisie.html
https://blogs.mediapart.fr/ahmed-chenikii/blog/230720/la-litterature-au-service-du-colonialisme