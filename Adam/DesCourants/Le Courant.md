*Implicitement la Révolution française : l'avènement du libéralisme et du républicanisme*
Le courant marginal, caché, augmente, augmente, jusqu'à devenir hégémonique. Il avait pris à la base, récupérant tout ce qui n'était pas contrôlé. Alors dominant, il travaille à envelopper toutes les pierres, jusqu'à ce que rien n'échappe à son flot, surveillant que d'autres courants ne prennent pas la même initiative. Tout doit être uniforme pour remplacer complètement l'ancien flux, rien ne doit rester.

*Le libéralisme*

*Ce qu'il faudrait pour le vaincre*

Et, de toutes façons…

Tu es cette goutte prise dans la vague, arrête la vague et la mer toute entière. Tu es ce rien qui va s'abattre sur la pierre, arrête cet acharnement lunaire. Tu es sous la forme minimale de ton existence, l'état fondamental qu'on retrouve lorsque toute énergie est puisée. Tu es ce résidu stable de l'après prélèvement. C'est tout le poids du monde qui t'écrase, te plie, te tord. Ces pseudos-lois universelles arbitraires qu'on t'impose, ce sont bien elles qui forment le contenant de ton essence.

Tu es cette goutte, arrête la vague…

Une goutte formant la vague qui vient s'abattre contre la pierre. Tous les jours, tous les matins, le flot de mineur s'engouffre dans les labyrinthes rocheux. Impulsions nerveuses, respiration haletante, battement de cœur, fléchissement du muscle, taillage de pierre et on recommence. 

Tu es cette goutte, arrête la vague…

**Note du 1er octobre**
Omg je suis tellement plus à l'aise ici maintenant mais c'est pas encore ça.
Le libéralisme c'est l'idée que nous sommes tous égaux, des gouttes d'eau
Le capitalisme c'est l'idée qu'on n'accepte qu'on ait pas tous la même taille et que c'est une bonne chose ??
L'universalisme c'est l'idée qu'il n'y a qu'une vague à suivre, tout ce qui ne suit pas tombe mais on va accompagner le mouvement. 