---
longform:
  format: scenes
  title: Courants Dominants
  workflow: Default Workflow
  sceneFolder: /
  scenes:
    - L'Homme
    - L'Empire
    - Le Frère
    - L'Ingénieur
    - L'Ouvrier
    - Le Courant
    - Les Territoires
    - Le Feu
    - La Maison, la Femme et le Fils
    - La Maladie
    - La Mort
    - Fin - La Lumière
  ignoredFiles:
    - Manuscript
---
