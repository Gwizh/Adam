Il faut pouvoir séparer en sous parties.

Le premier chapitre peut rester comme tel mais je pense que je peux ajouter des "scènes" intéressantes. --> Peut-être faire plus que ça.
Vraiment narrer ça comme une guerre et des batailles, avec toute la virilité sexiste que ça implique.  

### 1 - L'Enfant
Autodidacte rousseauiste avec pleins de bibliothèques.
Il faut lui trouver un modèle pour qu'il s'oriente vers le monde académique et universitaire.
Premiers articles dans les journaux locaux, ils financent des projets tehcnologiques. 
Premières ébauches de généralisation du savoir

#Question Quand est-ce que que commence le chapitre 2 alors ? Il a déjà quitté l'école ou il continue ?
J'imagine qu'il fait une partie en académie pendant le premier chapitre mais il part à cause de la dispute et c'est là qu'il crée son équipe et que commence vraiment son projet. Au début dans l'université puis dans le bâtiment dédié. Ou alors le chapitre 2 commence seulement au déménagement dans le labo.

### 2 - La Guerre 1 - L'Éclaireur
- Premières années, précarité (ou plutôt non rentabilité)
https://www.storys-international.com/post/les-%C3%A9volutions-des-int%C3%A9rieurs
https://www.storys-international.com/post/les-%C3%A9volutions-des-immeubles
- Transformateur
- Salon
- Publication d'un article qui fait grand bruit dans le monde scientifique : le tout électrique. Remplacer le gaz et la vapeur pour aller vers une autre révolution industriel (si le concept existait déjà). 
- Présentation Rival : un mix de Gould et Vanderbilt.
- Première victoire : le quartier alimenté par l'élec alternative ! Avec l'aide du transformateur qu'ils avaient mis tant de temps à créer. Grande fierté, scène de joie.
- Contrat avec le théâtre --> Consécration
**Il est toujours dominé par son concurrent mais a eu sa première grande victoire. Son système est dangereux et son entreprise est précaire**

#### Paternalisme dans ce chapitre
N'écoute pas les idées de ses collègues.
Première forme inconsciente de paternalisme

### 3 - La Guerre 2 - Le Technocrate
Drame : le gars meurt par le feu au théâtre. Comme twbb, chaque succès vient avec un drame.
- Parler de la bulle spéculative ?
- Parler des tests contre l'alternatif, propagande éhontée !
Phase de rigueur :
- Pôle expérimental avec toutes les innovations, big money
- Fin du chapitre : Battle de cables --> Victoire !
**Il a tenu le coup durant la mini crise, ses revenues et ses technologies sont stabilisés. Il n'est pas encore dominant mais son nom retentit. L'électricité en sort affaibli, sa popularité est diminuée.**

#### Paternalisme dans ce chapitre
Création d'un système de contrôle pour protéger les gens
Division des tâches, réduction de l'autonomie des travailleurs
Le but est de protéger chacun en les formant sur un seul aspect. Ne pas les laisser faire qq chose qu'ils ne savent pas faire.
Seulement sous la forme d'un concept, des blessures doivent arriver dans le chapitre comme preuve selon lui qu'il va dans le bon sens.
*Théorie et système imaginé*

### 4 - La Guerre 3 - Le Philantrope
Drame : Les ingénieurs sont écrasés par le stress et la charge de travail. Une part des chercheurs partent sans les sous car incapable de suivre le rythme effréné.
Deuxième série de complications
- Cette fois-ci politique, il faut parler de la réticence à s'engager par rapport aux américains qui n'ont pas peur du progrès.
- En plus, influence du lobby du gaz ?
Victoire au parlement, l'éclairage est professionnel et convient à tout le monde !
- Législation plus permissive
- Contrat avec l'université
- Compétition de mécénat et de collection d'art
- Optimisation de son modèle
**Comble de la gloire, tout le monde l'aime. Son système semble en parfaite adéquation avec le nouveau système économique et politique.**

#### Paternalisme dans ce chapitre
Recrutement d'une main d'oeuvre peu éduquée, rencontre d'Adam avec ce monde qu'il n'avait qu'imaginé dans ses calculs.
Il ressert encore le contrôle pour les "protéger", ne laisse rien passer.
L'usine est construite sur ces principes.
Il rencontre des gens qui ne font pas ça par passion mais pour le travail ainsi que d'autres qui veulent changer les règles sans avoir une image de comment le système complet fonctionnait.
Illumination des quartiers, même les plus pauvres, volonté de faire participer ce peuple à l'enteprise nationale. Civilisation par la construction d'usine et de logements par Adam. 
Empêcher au peuple d'aller dans les logements insalubres, déménagement de force.

### 5 - La Guerre 4 - L'Empereur
Drame : Grèves et contestations, le rythme est encore plus soutenu. Bennie se suicide
- Le continu revient à la charge. Installé depuis plus longtemps, il a plus de capital pour acheter des terres et exploite l'ignorance des paysans pour prendre beaucoup d'avance. De plus, le continu reprend l'idée terrible de solutions décentralisée, possédée par les aristos, ne pouvant pas être liés entre eux de manière universelle. Adam prône un retour du centralisme libéral d'une capitale éclairée (littéralement)
En même temps, tensions du fait de l'immigration et de la précarité des travailleurs. Partis populaires et agitateurs qui prennent du poids. 
- Création du pôle exportation avec la vente d'équipement de la Mulligan
Double message 
Adam prend les paysans pour des arriérés qui n'ont rien suivi à ce qu'il s'est passé à la capitale et au Parlement. De même il prend les travailleurs pour des ingrats qui ne voient pas les réels problèmes. Il pousse les gens pour concurrences les américains et les autres firmes. Les autres problèmes se résoudrons par ça ! Pas par autre chose !
Adam prône l'universalisme de sa solution.
- Il y arrive. Contrôle toutes les chaînes et à un quasi-monopole sur l'électricité dans le pays. Il a gagné !
**Il devient l'empereur à la tête d'un monopole immense qui tient malgré l'instabilité du pays. Son influence à l'étranger fait le rayonnement de la nation. Il meurt en héro et est décoré de son vivant pour ce qu'il a fait pour son pays.**

#### Paternalisme dans ce chapitre
Implantation à l'étranger, "des peuples encore moins civilisés". 
Darwinisme social raciste.

#### Idées à développer
Premier chapitre purement républicain, vantant les mérites de la société libérale capitaliste qui se sert de la raison et de la science pour permettre le progrès de la société.

L'auteur est absolument convaincu de ce système, il se considère humaniste et admire complètement cette figure historique et cette époque. 
Libéral : défense des libertés fondamentales
Capitalisme : système de domination historique, système "juste" basé sur une supposée loi de la nature
Républicanisme : civilisation européenne basée sur la science et la raison. pays des droits de l'homme et des libertés.

Pourtant, il faut comprendre dans ses mots que ce n'est pas aussi simple. Comment ? Avec des anachronismes de ma part mais qui sonnerait vrai dans ces mots ? Des trucs que nous savons maintenant comme faux mais qui aurait pu être vraissemblable à l'époque. En gros, faudrait donner des indices que ce n'est pas moi qui écrit.
Une solution à ce problème est juste de l'annoncer mdr

Sinon j'avais aussi :
    Préparation
    Duel
    Précipitation
    Rigueur
    Concurrence
    Universalisme