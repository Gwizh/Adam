### Partie 2
Trois idées principales. 
1. Romancier critique, voulant voir la vérité et qui reprend le bouquin de l'autre pour en apporter d'autres éléments.
2. On suit l'aventure d'un autre personnage qui va voir tous les autres
3. Tout le reste est intégré à l'histoire

#### 1
*...*
Ce que vous venez de lire est le travail d'un compère romancier. Un vieil ami de mon grand-père d'ailleurs. Ce roman a été publié et à été un peu oublié. Vous y avez cru à cette belle fable ? Adam Lee Mulligan, le grand héro de son temps ! Il semble que tout tourne autour de lui dans ce roman. Et c'est vrai ! C'est le personnage principal et quasiment le seul personnage d'ailleurs. C'est un récit qui est assez facile à lire tant il manque de rigueur pour assurer un récit continu. J'avais oublié l'existence de ce récit. Je l'ai retrouvé car il était cité sur l'article Wikipédia de ce cher Mulligan.

Pour rétablir un semblant de vrai, je vous propose une suite de petites histoires décrivant la vie des personnages autour de Mulligan. Je suis romancier moi-aussi, mais j'ai plus à coeur d'avoir un semblant de réalisme historique. Nous en avons appris beaucoup sur la vie de cet homme depuis sa mort en 1931 et la publication de ce roman atypique en 1972. L'auteur le dit lui-même, c'est un grand personnage, je pense être d'accord aussi. Moi-aussi je pense l'admirer mais ça ne m'empêche pas pour autant de remettre aux autres êtres vivants de son temps ce qui semblait être à César. Comme je l'ai dit, je suis romancier, j'ai lu une petite dizaine de bouquin d'histoire pour rendre tout ceci un peu plus solide mais mon art ne se veut pas juste. En ce sens, entre ce que vous avez lu et ce que vous lirez (ne fermez pas ce livre), il y a une différence de degré et pas de nature. Mais j'espère qu'en ajoutant un peu de substance dans cette histoire fantastique, on arrivera à se rapprocher au moins un peu du vrai.

Idée : Mettre des passages de narration entre chaque chapitre de la partie 2

C'est parti.

*6 - Les Inventeurices*
Histoire de Laura
- Idée que la lumiere nous "protege" en montrant ce qu'on nous dévoilait. Analogie avec les caméras d'aujourd'hui mais aussi nos téléphones : on montre ce qu'on veut montrer. Nahel.
- Voit l'impact de la pollution, est frappée par celle-ci, souhaite tres tot créer des normes pour encadrer la science sur un chemin respectueux du vivant. N'y arrive pas.

*7 - L'Administratrice*
Histoire d'Anna

*8 - L'Ingénieur*
Histoire de Bennie

*9 - L'Ouvrier*
Histoire de Richard

*10 - Le Feu*
Histoire de Felix

*11 - Le Gouverné*
Histoire de ...

*12 - Le Courant*
Histoire de John (?)