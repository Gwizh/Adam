Titre : Courants Dominants

Quatrième de couverture :
"Sécurité, Unité, Progrès !", cria Adam après avoir construit la plus grande usine de production d'électricité au monde. Adam Lee Mulligan est sûrement l'inventeur et l'entrepreneur le plus important de la Belle Époque. On lui doit l'illumination des grandes villes, à l'époque ou le prestige mondial passait par les grands monuments qui venaient percer le ciel. *Mdr n'imp*

Ce roman s'inspire de sa vie car elle a tellement pesé sur l'Histoire du monde qu'elle suffit presque à décrire l'époque et nos vies d'aujourd'hui.

Guerre des courants et Adam le boss !

*Old BS*
La fin du XIXème siècle en France est souvent connue comme étant "La Belle Époque". Une période de prospérité que certains osent même décrire comme l'apogée de la France moderne. Entre les très nombreuses innovations technologiques, artistiques et sociétales, on imagine vraiment un temps de développement et de paix qui a fait rayonner notre culture à l'international. C'était en effet le temps de l'instauration de la 3ème République, de la Marseillaise comme hymne national, de l'exposition universelle et sa Tour Eiffel, tous ces évènements convergeant quasiment au centenaire de la prise de la Bastille. Le bilan semble aussi positif dans les autres grandes puissances européennes. Au Royaume-Uni, l'époque victorienne reste le symbole d'une domination et d'une influence incontestée des îles brittaniques sur le monde. En Allemagne, la réunification récente déclencha une période de développement industriel sans précédent qui mit cette nouvelle nation dans le haut du classement mondial.

Mais on connaît tous sans le reconnaître complètement les raisons de cette prospérité idéalisée. Les inégalités sociales étaient profondes, le sexisme et le racisme omniprésents, la violence partout. La colonisation semble tâcher notre belle histoire, comme une aberration, une déviation involontaire, dans un bilan glorieux. Pourtant, je pense que les maux avaient des racines plus profondes. Ces "problèmes" n'étaient pas des manquements par rapport à une vertue immaculée de notre belle République. Ces problèmes étaient systémiques, inclus dans l'ADN même des structures de pouvoirs, intériorisés dans chacun de ses représentants. 

À travers un personnage, cet ouvrage souhaite décrire ces structures qui privilégiaient une certaine pensée, une certaine philosophie, un certain courant dominant.